﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demo9262017.EmailValidator_Refactored.Rules.Interface;
using System.Collections.Generic;
using Demo9262017.BestPractices.EmailValidator_Refactored;

namespace DemoTestProject
{
    [TestClass]
    public class EmailValidatorTests
    {
        [TestMethod]
        public void EmailValidator_EmailWithoutAnAtSignShouldBeInvalid()
        {
            // Arrange
            string testEmail = "test.com";
            bool expectedResult = false;

            var emailValidator = new EmailValidator(testEmail);

            // Act
            emailValidator.Validate();
            var result = emailValidator.IsValid();

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void EmailValidator_EmailNotEndingInComOrgEduShouldBeInvalid()
        {
            // Arrange
            string testEmail = "test@x.xxx";
            bool expectedResult = false;

            var emailValidator = new EmailValidator(testEmail);

            // Act
            emailValidator.Validate();
            var result = emailValidator.IsValid();

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void EmailValidator_PropertyFormattedEmailShouldBeValid()
        {
            // Arrange
            string testEmail = "jmoreland@selleractive.com";
            bool expectedResult = true;

            var emailValidator = new EmailValidator(testEmail);

            // Act
            emailValidator.Validate();
            var result = emailValidator.IsValid();

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        //[TestMethod]
        public void EmailValidator_ShouldReturnFalseIfAnyRulesReturnFalse()
        {
            // Arrange
            var rules = new List<IEmailRule>();

            // TODO: USE MOCKING FRAMEWORK TO CREATE EMAIL RULE THAT RETURNS FALSE
            // var fakeRule = MockingFramework.Create<IEmailRule>();
            // fakeRule.Validate(Arg.Any<string>()).Returns(false)

            var emailValidator = new EmailValidator("", rules);

            // Act
            emailValidator.Validate();
            var result = emailValidator.IsValid();

            // Assert
            Assert.AreEqual(result, false);
        }
    }
}
