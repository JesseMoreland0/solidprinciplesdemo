﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demo9262017.BestPractices.EmailValidator_Bad;
using Demo9262017.EmailValidator_Refactored.Rules;

namespace DemoTestProject
{
    [TestClass]
    public class EmailValidatorRulesTests
    {
        [TestMethod]
        public void EmailValidator_EmailWithoutAnAtSignShouldBeInvalid()
        {
            // Arrange
            var emailToTest = "kkk.test.com";

            var atSignRule = new AtSignRule();

            // Act
            var result = atSignRule.Validate(emailToTest);

            // Assert
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void EmailValidator_EmailWithAnAtSignShouldBeValid()
        {
            // Arrange
            var emailToTest = "kkk.test@com";

            var atSignRule = new AtSignRule();

            // Act
            var result = atSignRule.Validate(emailToTest);

            // Assert
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void EmailValidator_EmailLessThanTenCharacterShouldBeInvalid()
        {
            // Arrange
            var emailToTest = "j@k.com";

            var lengthRule = new LengthRule();

            // Act
            var result = lengthRule.Validate(emailToTest);

            // Assert
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void EmailValidator_Last4OfEmailShouldBeComOrgEdu()
        {
            // Arrange
            var emailToTest = "jmoreland@k.net";

            var last4OfEmailRule = new Last4OfEmailRule();

            // Act
            var result = last4OfEmailRule.Validate(emailToTest);

            // Assert
            Assert.AreEqual(result, false);
        }
    }
}
