﻿using Demo9262017.EmailValidator_Refactored.Rules.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9262017.EmailValidator_Refactored.Rules
{
    public class Last4OfEmailRule : IEmailRule
    {
        public string[] acceptedSuffixies = {".com", ".org", ".edu" };

        public string InvalidReason => "Email needs to end in .com / .edu / .org";

        public bool Validate(string email)
        {
            var last4OfEmail = email.Substring(email.Length - 4, 4);
            if (!acceptedSuffixies.Contains(last4OfEmail))
                return false;

            return true;
        }
    }
}
