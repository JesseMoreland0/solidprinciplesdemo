﻿using Demo9262017.EmailValidator_Refactored.Rules.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9262017.EmailValidator_Refactored.Rules
{
    public class AtSignRule : IEmailRule
    {
        public string InvalidReason { get { return "Email does not contain @"; } }

        public bool Validate(string email)
        {
            if (!email.Contains("@"))
            {
                return false;
            }

            return true;
        }
    }
}
