﻿using Demo9262017.EmailValidator_Refactored.Rules.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9262017.EmailValidator_Refactored.Rules
{
    public class LengthRule : IEmailRule
    {
        public string InvalidReason { get { return "Email is less than ten characters"; } }

        public bool Validate(string email)
        {
            if (email.Length < 10)
                return false;

            return true;
        }
    }
}
