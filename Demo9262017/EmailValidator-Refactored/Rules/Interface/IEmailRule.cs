﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9262017.EmailValidator_Refactored.Rules.Interface
{
    public interface IEmailRule
    {
        bool Validate(IEmail email);
        string InvalidReason { get; }
    }
}
