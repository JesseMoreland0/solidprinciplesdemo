﻿using Demo9262017.EmailValidator_Refactored.Rules;
using Demo9262017.EmailValidator_Refactored.Rules.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo9262017.BestPractices.EmailValidator_Refactored
{
    public class EmailValidator
    {
        string _email;
        bool _valid;

        private List<IEmailRule> _emailValidationRules = new List<IEmailRule>()
        {
            new AtSignRule(),
            new Last4OfEmailRule(),
            new LengthRule()
        };

        public List<String> invalidReasons = new List<string>();

        public EmailValidator(string email, List<IEmailRule> emailRules = null)
        {
            _email = email;
            _valid = false;

            if (emailRules != null)
                _emailValidationRules = emailRules;
        }

        public void Validate()
        {
            _valid = true;

            foreach(var rule in _emailValidationRules)
            {
                if (!rule.Validate(_email))
                {
                    invalidReasons.Add(rule.InvalidReason);
                    _valid = false;
                }
            }
        }

        public List<string> GetInvalidReasons()
        {
            return invalidReasons;
        }

        public bool IsValid()
        {
            return _valid;
        }
    }
}
