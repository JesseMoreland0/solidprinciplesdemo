﻿using Demo9262017.BestPractices.EmailValidator_Refactored;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9262017
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {

                Console.Write("Enter Email: ");
                var email = Console.ReadLine();

                var emailValidator = new EmailValidator(email); // Cannot create an Email Validator without giving it an email in the constructor

                emailValidator.Validate();

                if (emailValidator.IsValid())
                    Console.WriteLine("Cool. Valid email");
                else
                {
                    Console.WriteLine("Invalid Email");
                    foreach (var invalidReason in emailValidator.GetInvalidReasons())
                    {
                        Console.WriteLine(invalidReason);
                    }
                }

                Console.ReadLine();
            }
        }
    }
}
