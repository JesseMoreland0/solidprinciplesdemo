﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo9262017.BestPractices.EmailValidator_Bad
{
    public class EmailValidator
    {
        string _email;
        bool _valid;

        public List<String> invalidReasons = new List<string>();

        public EmailValidator(string email)
        {
            _email = email;
            _valid = false;
        }

        public void Validate()
        {
            _valid = true;

            // Make sure email contains an @ symbol
            if(!_email.Contains("@"))
            {
                _valid = false;
                invalidReasons.Add("Email does not contain @");
            }

            if(_email.Length < 6)
            {
                _valid = false;
                invalidReasons.Add("Email is less than six characters");
            }

            var last4OfEmail = _email.Substring(_email.Length - 4, 4);
            if (last4OfEmail != ".com" && last4OfEmail != ".edu" && last4OfEmail != ".org")
            {
                _valid = false;
                invalidReasons.Add("Email needs to end in .com/.edu/.org");
            }
        }

        public List<string> GetInvalidReasons()
        {
            return invalidReasons;
        }

        public bool IsValid()
        {
            return _valid;
        }
    }
}
