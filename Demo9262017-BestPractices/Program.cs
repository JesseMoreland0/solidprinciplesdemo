﻿using Demo9262017.BestPractices.EmailValidator_Bad;
using System;

namespace Demo9262017_BestPractices
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Email: ");
            var email = Console.ReadLine();

            var emailValidator = new EmailValidator(email);

            emailValidator.Validate();

            if (emailValidator.IsValid())
                Console.WriteLine("Cool. Valid email");
            else
            {
                Console.WriteLine("Invalid Email");
                foreach(var invalidReason in emailValidator.GetInvalidReasons())
                {
                    Console.WriteLine(invalidReason);
                }
            }


            Console.ReadLine();

        }
    }
}
